package com.sda.examples;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("George", "1234", 19);
        student.study();
        System.out.println();

        Student studen2 = new Student("Ana", "2345", 21);
        studen2.study();
        System.out.println();

        displayMessage();
    }

    public static void displayMessage() {
        System.out.println("Hello world!");
    }
}
