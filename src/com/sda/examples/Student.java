package com.sda.examples;

/**
 * class used for modelling a student.
 */
public class Student {
    private String name;
    private String CNP;
    private int age;

    /**
     * constructor whith parameters
     * @param nume - name of the student
     * @param CodNumericPersonal - CNP of the student
     */
    public Student(String nume, String CodNumericPersonal, int a){
        name = nume;
        CNP = CodNumericPersonal;
        age = a;
    }

    /**
     * method used for student behavior.
     */
    public void study(){
        System.out.println(name + " is a student.");
        displayStudent();
    }

    /**
     * method for student statement
     */
     public void displayStudent(){
        System.out.println("Student name is: " + name + " and his CNP is: " + CNP);
         System.out.println("Age of student is: " + age);
         staticMethod();
    }

    public static void staticMethod(){
        System.out.println("Hello from static method!");

    }


}
