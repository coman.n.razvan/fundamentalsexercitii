package com.sda.GroceryShoppyng;

public class Products {
    private String name;
    private int price;

    public Products(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Products(String name) {
        this.name = name;
    }

    public Products(int price) {
        this.price = price;
    }

    public String show() {
        return name + " avand pretul: " + price;
    }

    public int getPrice() {
        return price;
    }
}
