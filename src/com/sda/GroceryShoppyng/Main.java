package com.sda.GroceryShoppyng;

import javax.swing.*;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int[] arr = new int[5];
        Products[] arrayProduse = new Products[5];
        arrayProduse[0] = new Products("Apa", 5);
        arrayProduse[1] = new Products("Suc", 10);
        arrayProduse[2] = new Products("Chips", 8);
        arrayProduse[3] = new Products("Bomboana", 3);
        arrayProduse[4] = new Products("Guma", 2);

        showMenu(arrayProduse);
        Products[] cart = addToCart(arrayProduse);
        System.out.println("Aficam cosul de cumparaturi:");
        displayCart(cart);
        System.out.println(cartTotalPrice(cart));
        payCart(cartTotalPrice(cart));
    }

    /**
     * Display product menu.
     * @param produse
     */
    public static void showMenu(Products[] produse) {
        System.out.println("Type the number for the product you want: \n");

        for (int i = 0; i < produse.length; i++) {
            System.out.println(i + " for: " + produse[i].show());
        }
    }

    /**
     * Adaugam produse in cos si facem suma produselir din cos.
     *
     * @return suma preturilor produselor din cos.
     */
    public static Products[] addToCart(Products[] arrayProduse) {
        Scanner scanner = new Scanner(System.in);
        Products[] cart = new Products[5];
        int i = 0;
        int optiune = 0;
        while (i < cart.length) {
            optiune = scanner.nextInt();
            if (optiune < 0 || optiune >= arrayProduse.length) {
                System.out.println("Optiune invalida");
                continue;
            }
            cart[i] = arrayProduse[optiune];
            i++;
        }
        return cart;
    }

    /**
     * Afisare cos cumparaturi
     *
     * @param cart
     */
    public static void displayCart(Products[] cart) {
        for (int i = 0; i < cart.length; i++) {
            System.out.println(cart[i].show());
        }
    }

    /**
     * calculam total cos cumparaturi.
     *
     * @param cart
     * @return total cos cumparaturi.
     */
    public static int cartTotalPrice(Products[] cart) {
        int sum = 0;
        for (int i = 0; i < cart.length; i++) {

            sum += cart[i].getPrice();
        }
        return sum;
    }

    /**
     * Requesting cartPrice from user.
     *
     * @param cartTotalPrice
     */
    public static void payCart(int cartTotalPrice) {
        int userInput = scanner.nextInt();
        while (userInput != cartTotalPrice) {
            System.out.println("Valoare gresita, introduceti din nou: ");
            userInput = scanner.nextInt();
        }
        System.out.println("Cumparaturi reusite.");
    }
}
